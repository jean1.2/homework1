package com.shell.command.java;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class ShellCmd {
    private static class StreamGobbler implements Runnable {
        private InputStream inputStream;
        private Consumer<String> consumer;

        public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }

        @Override
        public void run() {
            try {
                new BufferedReader(new InputStreamReader(inputStream, "euc-kr")).lines()
                        .forEach(consumer);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        boolean isWindows = System.getProperty("os.name")
                .toLowerCase().startsWith("windows");

        //ProcessBuilder builder = new ProcessBuilder();

        List<String> cmd = new ArrayList<String>();
        cmd.add("cmd.exe");
        cmd.add("d:");
        cmd.add("cd D:\\User Storage\\WebProgramming\\IntelliJ Projects\\java\\src\\main\\java\\com\\shell\\command\\java");
        cmd.add("javac Gugudan.java");
        cmd.add("arg1");
        cmd.add("arg2");
        ProcessBuilder bld = new ProcessBuilder(cmd);
        bld.directory(new File("d:/User Storage/WebProgramming/IntelliJ Projects/java/src/main/java/com/shell/command/java"));
        Process process = bld.start();

      /*  if (isWindows) {
            builder.command("cmd.exe", "/d", "dir");
        } else {
            builder.command("sh", "-c", "ls");
        }
        builder.directory(new File(System.getProperty("user.home")));
        Process process = builder.start();*/
        StreamGobbler streamGobbler =
                new StreamGobbler(process.getInputStream(), System.out::println);
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        int exitCode = process.waitFor();
        assert exitCode == 0;
    }
}
