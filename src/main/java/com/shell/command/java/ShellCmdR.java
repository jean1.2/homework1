package com.shell.command.java;

import java.io.*;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class ShellCmdR {
    private static class StreamGobbler implements Runnable {
        private InputStream inputStream;
        private Consumer<String> consumer;

        public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }


        public void run() {
            try {
                new BufferedReader(new InputStreamReader(inputStream, "euc-kr")).lines()
                        .forEach(consumer);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

/*     private static String gugudan(){
        String str = "";
           for(int i=2; i<10; i+=2) { // 짝수 단만 2, 4, 6, 8
            for(int j=1; j<10; j++) {
                str += i + " x " + j + " = " + i*j;
               // System.out.println(i + " x " + j + " = " + i*j);
                if(j>=i)    // i = 2 일 때, j = 1, 2 까지 출력해서 반복문 탈출하기 위한 조건
                    break;    // 4, 6, 8 단도 마찬가지
            }
        }
           return str;
    }*/

    public static void main(String[] args) throws IOException, InterruptedException {
        boolean isWindows = System.getProperty("os.name")
                .toLowerCase().startsWith("windows");




        /**
         * 간단하게 Runtime 클래스를 사용해서 현재 사용자 디렉토리 정보를 가져와 출력하는 쉘 커멘드를 호출하는 예제
         */
        String Directory = System.getProperty("user.dir");
        System.out.println(Directory);
        Process process;
        if (isWindows) {
            process = Runtime.getRuntime()
                    .exec(String.format("cmd.exe /d:\\r\\n cd %s \\r\\n javac Gugudan.java \\r\\n java Gugudan", Directory));
        } else {
            process = Runtime.getRuntime()
                    .exec(String.format("sh -c ls %s", Directory));
        }

        /**
         * 쉘 프로세스를 실행하고 그 프로세스에 inputstream을 연결하고 컨슈머로 System.out::println를 넘긴다.
         */
        String psRetMsg = "";
        StreamGobbler streamGobbler =
                new StreamGobbler(process.getInputStream(), (item)->{
                    System.out.println(item);
                });
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        int exitCode = process.waitFor();
        assert exitCode == 0;
    }
}
